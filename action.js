'use strict';

const windowElem = document.querySelector('.window');
const closeBtn = document.querySelector('#windowClose');
const headerElem = document.querySelector('.window__header');

const wrapper = document.querySelector('.wrapper');
const modal = document.querySelector('.modal');
const inp = document.querySelector('#field');
const inpBtn = document.querySelector('#inpBtn');
const modaleCloseBtn = document.querySelector('#modalClose');
const help = document.querySelector('.help');

const folder = document.querySelectorAll('.folder');

let offsetX = 0;
let offsetY = 0;

closeBtn.addEventListener('click', closeWindow);
headerElem.addEventListener('mousedown', onMouseDown);
document.addEventListener('mouseup', onMouseUp);

modaleCloseBtn.addEventListener('click', closeModal);
wrapper.addEventListener('click', closeModal);
modal.addEventListener('click', stopProp);
inp.addEventListener('contextmenu', onContextMenu);
inp.addEventListener('keydown', onKeyDown);
inpBtn.addEventListener('click', seveToStorage);

folder[0].addEventListener('dblclick', openModal);

const stor = localStorage.getItem('inputValue');
const parseStor = JSON.parse(stor);
console.log(parseStor);  

function closeWindow() { 
    windowElem.style.display = 'none';
}

function closeModal() {
    wrapper.style.display = 'none';
}

function stopProp(event) {
    event.stopPropagation();
}

function onMouseDown(event) {
    offsetX = event.offsetX;
    offsetY = event.offsetY;
    document.addEventListener('mousemove', onMouseMove);
    windowElem.classList.add('move');
}

function onMouseMove(event) {
    windowElem.style.left = event.pageX - offsetX + 'px';
    windowElem.style.top = event.pageY - offsetY + 'px';
}

function onMouseUp() {
    document.removeEventListener('mousemove', onMouseMove);
    windowElem.classList.remove('move');

    const windowElemTop = parseInt(windowElem.style.top);
    const screenHeight = document.documentElement.clientHeight;
    const windowElemHalf = windowElem.clientHeight / 2;

    if (windowElemTop + windowElemHalf > screenHeight / 2) {
        windowElem.style.top = screenHeight - windowElem.clientHeight + 'px';
    } else {
        windowElem.style.top = 0;
    }
}

function onContextMenu(event) {
    event.preventDefault();
}

function onKeyDown(event) {
    if (inp.value.length > 10 && event.key !== 'Backspace') {
        inp.readOnly = true;
        help.style.opacity = 1;
    } else {
        inp.readOnly = false;
        help.style.opacity = 0;
    }
}

function openModal() {
    wrapper.style.display = 'flex';
}

function seveToStorage() {
    const text = inp.value;
    inp.value = '';

    localStorage.setItem('inputValue', JSON.stringify(text));
}


